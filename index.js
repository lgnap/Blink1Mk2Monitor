const displaySystem = require('./lib/display-system')
const checkers = require('./lib/checkers')
const events = require('./lib/events')
const log = require('npmlog')
const dotenv = require('dotenv')
const fs = require('fs')
const moment = require('moment')

const dotenvResult = dotenv.config()
if (dotenvResult.error) {
  throw dotenvResult.error
}

const LOG_FORMAT = 'YYYY-MM-DD HH:mm:ss'

const envConfig = dotenv.parse(fs.readFileSync('.env.example'))
for (let envKey in envConfig) {
  if (typeof process.env[envKey] === 'undefined') {
    throw new Error('Missing config key into .env file: ' + envKey)
  }
}

// allow node to quit properly
process.on('SIGINT', () => {
  events.stopListen()
  checkers.unloadCheckers()
  displaySystem.close()
  process.exit(0)
})
// allow node to crash when a promise is not catched - relaunched through pm2
process.on('unhandledRejection', up => {
  displaySystem.close()
  checkers.unloadCheckers()
  throw up
})

log.level = process.env.LOG_LEVEL
log.heading = '' + moment().format(LOG_FORMAT)
setInterval(() => {
  log.heading = '' + moment().format(LOG_FORMAT)
}, 1000)

events.listen()

displaySystem.init()

checkers.loadInitCheckers()
