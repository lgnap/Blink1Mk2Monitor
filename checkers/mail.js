const MailMonit = require('../lib/mail-monit')
const log = require('npmlog')
const events = require('../lib/events')

module.exports = {
  name: 'mail',
  init,
  unload
}

let mailMonitors = []

function init () {
  const mailCounter = parseInt(process.env.MAIL_COUNTER, 10)
  for (let i = 0; i < mailCounter; i++) {
    const mailConfig = JSON.parse(process.env['MAIL_' + i])
    log.verbose('mail', 'Init mail monitor for \'%s\'', mailConfig.id)
    mailMonitors.push(new MailMonit(mailConfig, (unreadCount) => {
      events.emit(unreadCount, module.exports.name + ':' + mailConfig.id)
    }))
  }
}

function unload () {
  mailMonitors.forEach((mailMonitor) => {
    mailMonitor.close()
  })
}
