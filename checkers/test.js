const events = require('../lib/events')

module.exports = {
  name: 'test',
  init,
  unload
}

let intervalId

function init () {
  intervalId = setInterval(() => {
    const date = new Date()

    return events.emit(date.getTime() % 3 === 0, module.exports.name)
  }, process.env.POOLING_INTERVAL)
}

function unload () {
  clearInterval(intervalId)
}
