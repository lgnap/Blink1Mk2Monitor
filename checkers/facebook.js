const log = require('npmlog')
const fb = require('../lib/facebook')
const events = require('../lib/events')

module.exports = {
  name: 'facebook',
  init,
  unload
}

let disableListen
let timeoutId

/**
 * Begin with a classic periodic state
 * (will fallback auto on listen)
 */
function init () {
  fb.getLogin().then(periodicState)
}

function unload () {
  if (disableListen) {
    disableListen()
  }
  if (timeoutId) {
    clearTimeout(timeoutId)
  }
}

/**
 * Listening to facebook
 * The callback is called when a new message arrive
 */
function enableListen (api) {
  log.silly('fb', 'enable listen')
  disableListen = api.listen(function (err, notifications) {
    // got a message notification
    if (err) {
      throw err
    }
    log.info('fb', 'listen message received %j', notifications)

    // disable listen
    disableListen()
    // ask for a refresh to periodic state
    periodicState(api)
  })
}

/**
 * Ask a state from FB and while:
 *  - it has at least a pending message => restart a refresh after pool time
 *  - it hasn't pending message => fall back to listen mode (less consuming)
 */
function periodicState (api) {
  log.silly('fb', 'periodic state')

  return fb.getUnreadThreadList(api).then(fb.getThreadsCount).then((counter) => {
    log.silly('fb', 'nb threads: %d', counter)
    events.emit(counter, module.exports.name)

    if (counter) {
      log.silly('fb', 'Unread message(s): %j', counter)
      // pool again
      timeoutId = setTimeout(() => { periodicState(api) }, process.env.POOLING_INTERVAL)
    } else {
      // reenable listen
      enableListen(api)
    }
  })
}
