const pmx = require('pmx')
const displaySystem = require('./display-system')
const config = require('config')

module.exports = {
  notificationOn,
  notificationOff
}

const defaultColor = 'ff9900'

const notifiersMapping = config.get('notifiers')

function convertNameToConfig (name) {
  if (notifiersMapping.hasOwnProperty(name)) {
    return notifiersMapping[name]
  }

  if (name.indexOf(':') !== -1) {
    const parametrizedName = name.split(':')

    if (notifiersMapping.hasOwnProperty(parametrizedName[0])) {
      if (notifiersMapping[parametrizedName[0]].hasOwnProperty(parametrizedName[1])) {
        return notifiersMapping[parametrizedName[0]][parametrizedName[1]]
      }
      return notifiersMapping[parametrizedName[0]]['default']
    }
  }

  return { blink: 0, color: defaultColor }
}

function notificationOn (name) {
  const config = convertNameToConfig(name)
  // log.silly('notifier', 'notification on for %s (color: %s)', name, config.color)
  pmx.emit('notification:on', { name, blink: config.blink, color: config.color })

  displaySystem.addColor(config.blink, config.color)
}

function notificationOff (name) {
  const config = convertNameToConfig(name)
  // log.silly('notifier', 'notification off for %s (color: %s)', name, config.color)
  pmx.emit('notification:off', { name, blink: config.blink, color: config.color })

  displaySystem.removeColor(config.blink, config.color)
}
