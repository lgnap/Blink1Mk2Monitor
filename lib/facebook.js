const Promise = require('bluebird')
const fbLogin = require('facebook-chat-api')

module.exports = {
  getLogin,
  getThreadsCount,
  getUnreadThreadList
}

const THREAD_LIST_LIMIT_MAX = 15 // You never let 15 unread fb conversations
const login = Promise.promisify(fbLogin)

let loginPromise = null

/**
 * Login to Facebook
 */
function getLogin () {
  if (loginPromise) {
    return loginPromise
  }
  loginPromise = login({ email: process.env.FB_LOGIN, password: process.env.FB_PASSWORD })
  return loginPromise.then(setOptions)
}

function setOptions (api) {
  api.setOptions({
    logLevel: 'silent'
  })
  return api
}
/**
 * Get inbox unread threads
 */
function getUnreadThreadList (api) {
  return Promise.promisify(api.getThreadList)(
    THREAD_LIST_LIMIT_MAX,
    null,
    [
      'INBOX',
      'unread'
    ]
  )
}

/**
 * For the moment, it's a simple logic.
 * But I'll see a more intelligent notification according to thread's ids and/or numbers of unreads into thread
 */
function getThreadsCount (threads) {
  return threads.length
}
