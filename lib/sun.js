const suncalc = require('suncalc')
const moment = require('moment')
const dateProvider = require('./date-provider')

module.exports = {
  isNight,
  isDay
}

function getLocation () {
  const geoloc = process.env.PLACE_GEOLOC.split(',')

  return {
    latitude: geoloc[0],
    longitude: geoloc[1]
  }
}

function getTimes () {
  const { latitude, longitude } = getLocation()
  return suncalc.getTimes(dateProvider.getNow(), latitude, longitude)
}

function isNight () {
  return !isDay()
}

function isDay () {
  const times = getTimes()
  const now = moment(dateProvider.getNow())
  return now.isAfter(times.sunrise) && now.isBefore(times.sunsetStart)
}
