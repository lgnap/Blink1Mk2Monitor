const Blinks = require('./blinks')
const Promise = require('bluebird')
const log = require('npmlog')

module.exports = {
  init,
  close,
  addColor,
  removeColor,
  enableErrorMode
}

const DISPLAY_TIME_LED = 2000

const COLOR_RED = 'FF0000'

const LED_ONE = 1
const LED_TWO = 2

let blinkStatus = []
let blink

blinkStatus[0] = []
blinkStatus[1] = []

function enableErrorMode () {
  return Promise.all([
    blink.switchOn(LED_ONE, COLOR_RED),
    blink.switchOn(LED_TWO, COLOR_RED)
  ])
}

function removeColor (blinkId, hexColor) {
  // if the led has not the color, you don't have color to remove
  const blinkIndex = blinkStatus[blinkId].indexOf(hexColor)
  if (blinkIndex === -1) {
    return
  }

  blinkStatus[blinkId].splice(blinkIndex, 1)
  log.silly('display-system', 'removeColor - blinkStatus: %j', blinkStatus)
}

function addColor (blinkId, hexColor) {
  // if the led has always the color stop process
  if (blinkStatus[blinkId].indexOf(hexColor) !== -1) {
    return
  }

  blinkStatus[blinkId].push(hexColor)
  log.silly('display-system', 'addColor - blinkStatus: %j', blinkStatus)
}

function displayLoop () {
  const ledPromises = blinkStatus.map((blinkColors, ledId) => {
    if (!blinkColors.length) {
      return blink.switchOff(ledId + 1)
    }

    return displayLed(ledId, Array.from(blinkColors))
  })

  return Promise.all(ledPromises)
}

function displayLed (ledId, colorsToDisplay) {
  if (!colorsToDisplay.length) {
    return Promise.resolve('Led ' + ledId + ': empty ')
  }

  return blink.switchOn(ledId + 1, colorsToDisplay[0]).then(() => {
    return Promise.delay(DISPLAY_TIME_LED).then(() => {
      return displayLed(ledId, colorsToDisplay.slice(1))
    })
  })
}

function periodicRefreshDisplayLoop () {
  return displayLoop().then(periodicRefreshDisplayLoop)
}

function init () {
  blink = new Blinks()
  periodicRefreshDisplayLoop()
}

function close () {
  blink.close()
}
