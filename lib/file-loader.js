module.exports = {
  requireModule: (modulePath) => {
    return require(modulePath)
  }
}
