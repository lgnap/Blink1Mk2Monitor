const checkersFolder = '/checkers/'
const fs = require('fs')
const Promise = require('bluebird')
const log = require('npmlog')
const fileLoader = require('./file-loader')

module.exports = {
  loadInitCheckers,
  unloadCheckers
}

function loadInitCheckers () {
  return loadCheckers().then((checkers) => {
    checkers.forEach(initChecker)
  })
}

function unloadCheckers () {
  return loadCheckers().then((checkers) => {
    checkers.forEach(unloadChecker)
  })
}

function loadCheckers () {
  const fullCheckersPath = process.cwd() + checkersFolder
  return Promise.promisify(fs.readdir)(fullCheckersPath).then((files) => {
    return files.map(file => {
      return fileLoader.requireModule(fullCheckersPath + file)
    })
  })
}

function initChecker (checker) {
  const enabledCheckers = process.env.CHECKERS_ENABLED.split(',')

  if (enabledCheckers.indexOf(checker.name) === -1) {
    log.warn('checker', 'Module %s is not listed as enabled in .env', checker.name)
    return
  }
  log.info('checker', 'Module %s is enabled in .env', checker.name)

  checker.init()
}

function unloadChecker (checker) {
  if (!checker.enabled) {
    log.silly('checker', 'Module %s is disabled', checker.name)
    return
  }

  checker.unload()
}
