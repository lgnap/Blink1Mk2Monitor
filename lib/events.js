const events = require('events')
const log = require('npmlog')
const notifiers = require('./notifiers')

// Create an eventEmitter object
const eventEmitter = new events.EventEmitter()

module.exports = {
  emit,
  listen,
  stopListen
}

function listen () {
  eventEmitter.on('toggleOn', function ({ name }) {
    log.silly('event', 'toggle on for %s', name)
    notifiers.notificationOn(name)
  })

  eventEmitter.on('toggleOff', function ({ name, color }) {
    log.silly('event', 'toggle off for %s', name)
    notifiers.notificationOff(name)
  })
}

function stopListen () {
  eventEmitter.removeAllListeners('toggleOn')
  eventEmitter.removeAllListeners('toggleOff')
}

function emit (unreadCount, name) {
  log.silly('event', 'emit received for %s with param %d', name, unreadCount)

  eventEmitter.emit(
    unreadCount ? 'toggleOn' : 'toggleOff',
    { unreadCount, name }
  )
}
