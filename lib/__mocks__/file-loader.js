const path = require('path')
const fl = jest.genMockFromModule('../file-loader')

fl.requireModule.mockImplementation((modulePath) => {
  return {
    name: path.basename(modulePath),
    init: jest.fn(),
    unload: jest.fn(),
    enabled: modulePath.indexOf('file1') !== -1
  }
})

module.exports = fl
