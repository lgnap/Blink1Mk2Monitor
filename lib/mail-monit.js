const Imap = require('imap')
const Promise = require('bluebird')
const log = require('npmlog')

const BLOCK_INFINITE_EVENT_LOOP = 500

class MailMonit {
  constructor (imapOptions, callback) {
    this.closeOpenAllow = true
    this.imap = new Imap(imapOptions)
    this.callback = callback

    this.imap.once('ready', () => {
      this.getUnreadCount('INBOX').then((unseenCount) => {
        this.callback(unseenCount)

        this.installUpdatesCallbackOn('INBOX')
      })
    })

    this.imap.connect()
  }

  getUnreadCount (boxName) {
    return new Promise((resolve, reject) => {
      this.imap.status(boxName, (err, boxStatus) => {
        if (err) {
          reject(err)
          return
        }

        log.silly('mail-monit', 'Messages from box \'%s\': %j', boxName, boxStatus.messages)

        if (boxStatus.messages.unseen) {
          log.info('mail-monit', 'Unread message(s): %d', boxStatus.messages.unseen)
        }

        resolve(boxStatus.messages.unseen)
      })
    })
  }

  installUpdatesCallbackOn (boxName) {
    this.imap.openBox(boxName, true, (err) => {
      if (err) { throw err }

      this.imap.on('update', this.closeOpenStatusBox.bind(this))
      this.imap.on('mail', this.closeOpenStatusBox.bind(this))
    })
  }

  closeOpenStatusBox () {
    if (!this.closeOpenAllow) {
      return
    }
    this.closeOpenAllow = false

    setTimeout(() => {
      this.closeOpenAllow = true
    }, BLOCK_INFINITE_EVENT_LOOP)

    this.imap.closeBox((err) => {
      if (err) { throw err }

      this.getUnreadCount('INBOX').then((unseenCount) => {
        this.callback(unseenCount)

        this.imap.openBox('INBOX', true, (err) => {
          if (err) { throw err }
        })
      })
    })
  }

  close () {
    this.imap.once('end', () => {
      console.log('Connection ended')
    })

    this.imap.close()
  }
}

module.exports = MailMonit
