const WrapperNodeBlink = require('./wrapper-node-blink1')

const util = require('util')
const log = require('npmlog')
const color = require('./color')
const sun = require('./sun')

const FADE_TIME = 750

class Blinks {
  constructor () {
    try {
      this.wrapper = new WrapperNodeBlink()
    } catch (err) {
      throw new Error('Error on opening (' + err.message + ')')
    }
  }

  switchOn (ledId, hexColor) {
    log.silly('blink', 'switchOn(%d, %s)', ledId, hexColor)
    const { r, g, b } = color[sun.isDay() ? 'hex2dec' : 'hex2decLimited'](hexColor)

    return this.wrapper.fadeToRGB(FADE_TIME / 2, r / 2, g / 2, b / 2, ledId).then(() => {
      return this.wrapper.fadeToRGB(FADE_TIME / 2, r, g, b, ledId)
    }).then(() => {
      return util.format('switched on(%d, %s)', ledId, hexColor)
    })
  }

  switchOff (ledId) {
    log.silly('blink', 'switchOff(%d)', ledId)
    return this.wrapper.fadeToRGB(FADE_TIME, 0, 0, 0, ledId).then(() => {
      return util.format('switched off(%d)', ledId)
    })
  }

  close () {
    log.silly('blink', 'close')
    return this.wrapper.off().then(() => {
      return this.wrapper.close()
    })
  }
}
// device: a usb key with it's two leds
// led: two leds on one device
// blink: a device's led

module.exports = Blinks
