const convert = require('color-convert')

module.exports = {
  hex2dec,
  rgb2hsv,
  hex2decLimited
}

function hex2dec (hexColor) {
  const rgb = convert.hex.rgb(hexColor)

  return {
    r: rgb[0],
    g: rgb[1],
    b: rgb[2]
  }
}

function rgb2hsv ({ r, g, b }) {
  const hsv = convert.rgb.hsv(r, g, b)

  return {
    h: hsv[0],
    s: hsv[1],
    v: hsv[2]
  }
}

function hex2decLimited (hexColor) {
  let { h, s, v } = rgb2hsv(hex2dec(hexColor))

  v = Math.min(v, process.env.COLOR_BRIGHTNESS_LIMIT)

  const rgb = convert.hsv.rgb(h, s, v)

  return {
    r: rgb[0],
    g: rgb[1],
    b: rgb[2]
  }
}
