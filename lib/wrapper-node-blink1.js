const NodeBlink1 = require('node-blink1')
const Promise = require('bluebird')

class Wrapper {
  constructor () {
    this.nodeBlink = new NodeBlink1()
    Promise.promisifyAll(this.nodeBlink)
  }
  fadeToRGB (fadeMillis, r, g, b) {
    return this.nodeBlink.fadeToRGBAsync(fadeMillis, r, g, b)
  }
  off () {
    return this.nodeBlink.offAsync()
  }
  close () {
    return this.nodeBlink.closeAsync()
  }
}

module.exports = Wrapper
