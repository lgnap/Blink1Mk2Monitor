const mock = jest.fn().mockImplementation(() => {
  // TODO : dont mock async and use the promisifyAll
  return {
    // we ignore the job done by promisifyAll
    // We are defining into mock the functions already promisified
    fadeToRGBAsync: jest.fn().mockResolvedValue('fadeToRGB'),
    offAsync: jest.fn().mockResolvedValue('off'),
    closeAsync: jest.fn().mockResolvedValue('close')
  }
})

module.exports = mock
