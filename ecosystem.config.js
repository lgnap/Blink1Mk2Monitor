const deployment = {
  host: [],
  ref: 'origin/master',
  repo: 'https://gitlab.com/lgnap/Blink1Mk2Monitor.git',
  path: '/srv/app',
  'post-deploy': 'npm install --only=prod && pm2 reload ecosystem.config.js --env production'
}
module.exports = {
  apps: [
    {
      name: 'blink1-notification',
      script: 'index.js',
      env_production: {
        NODE_ENV: 'production'
      },
      error_file: '/dev/null',
      out_file: '/dev/null'
    }
  ],

  deploy: {
    eink: {
      ref: deployment.ref,
      repo: deployment.repo,
      path: deployment.path,
      'post-deploy': deployment['post-deploy'],
      host: ['eink']
    },
    volumio: {
      ref: deployment.ref,
      repo: deployment.repo,
      path: deployment.path,
      'post-deploy': deployment['post-deploy'],
      host: ['volumio']
    },
    production: {
      ref: deployment.ref,
      repo: deployment.repo,
      path: deployment.path,
      'post-deploy': deployment['post-deploy'],
      host: ['eink', 'volumio']
    }
  }
}
