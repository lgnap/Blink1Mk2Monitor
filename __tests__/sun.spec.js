const sun = require('../lib/sun')
/* global jest describe test expect */

jest.mock('../lib/date-provider')
const dateProvider = require('../lib/date-provider')
process.env.PLACE_GEOLOC = '50.920773,4.458900' // this is not my home don't dream ...

/*
dawn: 2018-06-15T02:42:41.443Z, => premiers rayons indirects du soleil
sunrise: 2018-06-15T03:28:57.440Z,
sunriseEnd: 2018-06-15T03:33:21.479Z,

sunset: 2018-06-15T19:58:57.345Z,
sunsetStart: 2018-06-15T19:54:33.306Z,
*/

describe('Is night', () => {
  test('Into', () => {
    dateProvider.getNow.mockImplementation(() => new Date(2018, 5, 15, 22, 55, 0)) // each time - 2h (GMT)
    expect(sun.isNight()).toBe(true)
  })

  test('Outside', () => {
    dateProvider.getNow.mockImplementation(() => new Date(2018, 5, 15, 8, 0, 0)) // each time - 2h (GMT)
    expect(sun.isNight()).toBe(false)
  })
})

describe('Is day', () => {
  test('Into', () => {
    dateProvider.getNow.mockImplementation(() => new Date(2018, 5, 15, 12, 30, 0)) // each time - 2h (GMT)
    expect(sun.isDay()).toBe(true)
  })

  test('Outside day', () => {
    dateProvider.getNow.mockImplementation(() => new Date(2018, 5, 15, 22, 55, 0)) // each time - 2h (GMT)
    expect(sun.isDay()).toBe(false)
  })
})
