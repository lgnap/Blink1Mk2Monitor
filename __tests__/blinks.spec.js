const Blinks = require('../lib/blinks')

jest.mock('../lib/sun')

describe('Blinks', () => {
  let blinks
  beforeEach(() => {
    blinks = new Blinks()
  })

  afterEach(() => {
    return blinks.close()
  })

  test('switchOn', () => {
    expect.assertions(1)
    return expect(blinks.switchOn(1, '00FF00')).resolves.toBe('switched on(1, 00FF00)')
  })

  test('switchOff', () => {
    expect.assertions(1)
    return expect(blinks.switchOff(1)).resolves.toBe('switched off(1)')
  })

  test('close', () => {
    expect.assertions(1)
    return expect(blinks.close()).resolves.toBe('close')
  })
})
