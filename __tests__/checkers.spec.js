const checkers = require('../lib/checkers')

jest.mock('../lib/file-loader')
const fileLoader = require('../lib/file-loader')
jest.mock('bluebird')
const bluebird = require('bluebird')

bluebird.promisify.mockImplementation(() => {
  return jest.fn().mockResolvedValue(['file1', 'file2'])
})

process.env.CHECKERS_ENABLED = 'file1'

describe('checkers process', () => {
  test('loadInitCheckers', () => {
    return checkers.loadInitCheckers().then(function () {
      const mock = fileLoader.requireModule.mock
      expect(mock.results).toHaveLength(2)

      expect(mock.results[0].value.name).toEqual('file1')
      expect(mock.results[1].value.name).toEqual('file2')
    })
  })

  test('unloadCheckers', () => {
    return checkers.unloadCheckers().then(function () {
      const mock = fileLoader.requireModule.mock

      // not isolated from test before :-(
      expect(mock.results).toHaveLength(4)

      expect(mock.results[0].value.name).toEqual('file1')
      expect(mock.results[1].value.name).toEqual('file2')
    })
  })
})
