const color = require('../lib/color')

/* global describe test expect */

describe('Color hex2dec', () => {
  test('test extreme colors', () => {
    expect(color.hex2dec('ffffff')).toEqual({ r: 255, g: 255, b: 255 })
    expect(color.hex2dec('000000')).toEqual({ r: 0, g: 0, b: 0 })
  })

  test('test one color', () => {
    expect(color.hex2dec('ff0000')).toEqual({ r: 255, g: 0, b: 0 })
    expect(color.hex2dec('00ff00')).toEqual({ r: 0, g: 255, b: 0 })
    expect(color.hex2dec('0000ff')).toEqual({ r: 0, g: 0, b: 255 })
  })

  test('test favorite color', () => {
    expect(color.hex2dec('ff9900')).toEqual({ r: 255, g: 153, b: 0 })
  })
})

describe('Color rgb2hsv', () => {
  test('test extreme colors', () => {
    expect(color.rgb2hsv({ r: 255, g: 255, b: 255 })).toEqual({ h: 0, s: 0, v: 100 })
    expect(color.rgb2hsv({ r: 0, g: 0, b: 0 })).toEqual({ h: 0, s: 0, v: 0 })
  })

  test('test one color', () => {
    expect(color.rgb2hsv({ r: 255, g: 0, b: 0 })).toEqual({ h: 0, s: 100, v: 100 })
    expect(color.rgb2hsv({ r: 0, g: 255, b: 0 })).toEqual({ h: 120, s: 100, v: 100 })
    expect(color.rgb2hsv({ r: 0, g: 0, b: 255 })).toEqual({ h: 240, s: 100, v: 100 })
  })

  test('test favorite color', () => {
    expect(color.rgb2hsv({ r: 255, g: 153, b: 0 })).toEqual({ h: 36, s: 100, v: 100 })
  })
})

describe('Color hex2decLimited', () => {
  process.env.COLOR_BRIGHTNESS_LIMIT = 20
  test('test extreme colors', () => {
    expect(color.hex2decLimited('ffffff')).toEqual({ r: 51, g: 51, b: 51 })
    expect(color.hex2decLimited('000000')).toEqual({ r: 0, g: 0, b: 0 })
  })

  test('test one color', () => {
    expect(color.hex2decLimited('ff0000')).toEqual({ r: 51, g: 0, b: 0 })
    expect(color.hex2decLimited('00ff00')).toEqual({ r: 0, g: 51, b: 0 })
    expect(color.hex2decLimited('0000ff')).toEqual({ r: 0, g: 0, b: 51 })
  })

  test('test favorite color', () => {
    expect(color.hex2decLimited('ff9900')).toEqual({ r: 51, g: 31, b: 0 })
  })
})
