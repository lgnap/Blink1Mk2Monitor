const dateProvider = require('../lib/date-provider')

describe('date provider', () => {
  test('now is ... now', () => {
    const now = new Date()
    expect(dateProvider.getNow()).toMatchObject(now)
  })
})
