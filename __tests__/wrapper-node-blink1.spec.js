const WrapperNodeBlink1 = require('../lib/wrapper-node-blink1')

describe('Wrapper node blink 1', () => {
  test('fadeToRGB', () => {
    const wrapper = new WrapperNodeBlink1()
    expect.assertions(1)
    return wrapper.fadeToRGB().then(function (result) {
      expect(result).toBe('fadeToRGB')
    })
  })
  test('off', () => {
    const wrapper = new WrapperNodeBlink1()

    expect.assertions(1)
    return wrapper.off().then(function (result) {
      expect(result).toBe('off')
    })
  })
  test('close', () => {
    const wrapper = new WrapperNodeBlink1()

    expect.assertions(1)
    return wrapper.close().then(function (result) {
      expect(result).toBe('close')
    })
  })
})
