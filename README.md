# Blink1 Mk2 monitor

[![pipeline status](https://gitlab.com/lgnap/Blink1Mk2Monitor/badges/master/pipeline.svg)](https://gitlab.com/lgnap/Blink1Mk2Monitor/commits/master)
[![coverage report](https://gitlab.com/lgnap/Blink1Mk2Monitor/badges/master/coverage.svg)](https://gitlab.com/lgnap/Blink1Mk2Monitor/commits/master)

## What is that?

This little node app allows you to drive a blink (or more) according to some notifiers.

For the moment I develop a notifier for facebook and another for mails.
I'd like to create more of course.


## Configuration 

Create a `.env` file with all params needed.
You can copy `.env.example` file and fill in.

### Checkers availables
 - facebook
 - mail
 - test

 For each checker, you need to add it's name into `CHECKERS_ENABLED` env, separated by a `,`

#### Facebook
Provide `FB_LOGIN` and `FB_PASSWORD`. We can't use API token due to the reverse enginering done by `facebook-chat-api` check it's doc to know more.

#### Mail

You can listen to any mail number you'd like.
You just have to create an entry `MAIL_*` (numbering begin with 0) for each mail.

`MAIL_*` is a simple JSON object containing all params needed to connect to email server
```
{
    "id": "id-used-for-color-referenced in config/default.json",
    "user": "imap-login@domain.org",
    "password": "the-password",
    "host": "imap-host",
    "port": 993,
    "tls": true
}
```

Increment `MAIL_COUNTER` by step of one (for each `MAIL_*` line)

#### Test

test only, don't use in production

## Starting

A simple `npm start` could make the thing, but it's designed to crashed when a issue happend (promise errors not handled), so take `pm2` to launch and maintain this service up & running.

## Any questions?

Don't hesitate to contact me on [Mastodon](https://mstdn.io/@lgnap) or through [Twitter](https://www.twitter.com/lgnap)
