module.exports = function () {
  return {
    files: [
      'lib/**/*.js',
      '__mocks__/**/*.js'
    ],

    tests: [
      '__tests__/**/*spec.js'
    ],

    env: {
      type: 'node',
      runner: 'node'
    },

    testFramework: 'jest'
  }
}
